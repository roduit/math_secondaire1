# Accueil

Bienvenue sur ce site destiné à accompagner mes élèves dans la découverte des maths durant leurs parcours au CO de la Glâne.


<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/deed.fr"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />Ce site est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/deed.fr">Licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 4.0 International</a>.

