---
title: NO 9.1 - Planification
author: Charles Rooduit
date: 2023
---

# 🚵 Planification 

C'est une prévision de planificaiton. Bien sûr, selon les activités, les questions, le travail, cette planificaiton sera adaptée durant le cours.

## Partie 1 - Les nombres naturels.

!!! objectifs "Objectifs travaillés"
    - [ ] Je maîtrise les livrets jusqu'à 12.
    - [ ] Je maîtrise le vocabulaire en lien avec les nombres et opérations
    - [ ] Je sais à quoi servent les parenthèses, ce qu'elles signifient.        
    - [ ] J'effectue correctuement une chaîne opératoire composé d'un mélange d'opérations et de parenthèses en respectant les priorités des opérations.

!!! exercices "Exercices concernés"

    NO1 à NO26

Qu'est-ce qu'un nombre naturel ? Rappel sur les opérations de base (addition, soustraction, multiplication, division)

## Partie 2 - Critères de divisibilité.

!!! objectifs "Objectifs travaillés"
    - [ ] Je sais si un nombre est divisible par 2,3,4,5,9,10 et 25 en utilisant les critères de divisibilité.

!!! exercices "Exercices concernés"
    
    NO27 à NO49

Comment reconnait-on si un nombre est divisible par un autre ? 

## Partie 3 - Les nombres premiers, ppmc et pgdc.

!!! objectifs "Objectifs travaillés"
    - [ ] Je reconnaît un nombre premier.
    - [ ] J'arrive à trouver le PPMC de plusieurs nombres.
    - [ ] J'arrive à trouver le PGDC de plusieurs nombres.

!!! exercices "Exercices concernés"
    
    NO50 à NO60
    
C'est quoi un nombre premier ?

## Partie 4 - Les puissances.

!!! objectifs "Objectifs travaillés"

    - [ ] Je sais lire et écrire les nombres sous forme de puissance (carré, cube)
    - [ ] Je reconnais quand les parenthèses sont indispensables ou lorsqu'elles aident à mieux identifier les différentes parties d'un calcul.
    - [ ] J'utilise les propriétés des opérations pour organiser les calculs, les rendre plus facile à effectuer et pour en donner des estimations.


!!! exercices "Exercices concernés"
    
    NO61 à NO81

Définition et utilisation des puissances.

## Partie 5 - Opérations avec des nombres décimaux.

!!! objectifs "Objectifs travaillés"
    - [ ] J'effectue correctuement une chaîne opératoire composé d'un mélange d'opérations et de parenthèses en respectant les priorités des opérations.
    - [ ] Je sais à quoi servent les parenthèses, ce qu'elles signifient.
    - [ ] Je reconnais quand les parenthèses sont indispensables ou lorsqu'elles aident à mieux identifier les différentes parties d'un calcul.
    - [ ] J'utilise les propriétés des opérations pour organiser les calculs, les rendre plus facile à effectuer et pour en donner des estimations.


!!! exercices "Exercices concernés"
    
    NO82 à NO90

Les nombres décimaux (à virgules).

## Partie 6 - Récapitulation.

Est-on à l'aise avec ce qu'on a vu ces 5 semaines ?

## Partie 7 - Test

Prêt pour le test !
