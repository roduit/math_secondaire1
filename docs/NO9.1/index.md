---
title: 🏡 Nombres et opérations
---

# NO 9.1 Nombres et opérations

## 🎯 Objectifs

??? success "Propriétés des nombres naturels"
    - [x] Je maîtrise les livrets jusqu'à 12.
    - [x] Je reconnaît un nombre premier.
    - [x] Je sais si un nombre est divisible par 2,3,4,5,9,10 et 25 en utilisant les critères de divisibilité.
    - [x] Je suis capable de décomposer un nombre en facteur de nombres premiers.
    - [x] J'arrive à trouver le PPMC de plusieurs nombres.
    - [x] J'arrive à trouver le PGDC de plusieurs nombres.
    
??? success "Les différentes écritures d'un nombre"
    - [x] Je maîtrise le vocabulaire en lien avec les nombres et opérations
    
        *Chiffre, nombre, nombre naturel, nombre décimal, ordre croissant, décroissant, multiple, diviseur, ppmc, pgdc, nombre premier, terme, facteur, diviseur, dividende, somme, différence, produit, quotient.*
        
    - [x] Je sais lire et écrire les nombres sous forme de puissance (carré, cube)

??? success "Priorités des opérations."
    - [x] J'effectue correctuement, sans calculatrice, une chaîne opératoire composé d'un mélange d'opérations et de parenthèses en respectant les priorités des opérations.
    - [x] Je sais à quoi servent les parenthèses, ce qu'elles signifient.
    - [x] Je reconnais quand les parenthèses sont indispensables ou lorsqu'elles aident à mieux identifier les différentes parties d'un calcul.
    - [x] J'utilise les propriétés des opérations pour organiser les calculs, les rendre plus facile à effectuer et pour en donner des estimations.
    
!!! competence "Compétences visées"

    - Mobiliser les savoirs, savoir-faire et savoir être pour résoudre des problèmes en lien avec les nombres naturels et décimaux.
    - Développer des stratégies de recherche afin de résoudre des problèmes.
    - Présenter les solutions afin que les autres puissent s’approprier ma démarche (Explications préalables, grandeur cherchée, unités, calculs, réponse).
    - Développer des stratégies d’apprentissages afin de s’approprier les savoirs et savoir-faire en jeu et pouvoir les mobiliser en situation.
    - Vérifier sa solution ou au moins sa plausibilité. 
