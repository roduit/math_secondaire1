# 💡Truc et astuces

Dans cette partie, tu trouveras des astuces pour calculer plus rapidement.

!!! success "Tu connais d'autres astuces ?"

    N'hésite pas à me les communiquer, je les ajouterai ici :smile:

## Division et multiplication de nombres à virgule

On peut transformer une division par un nombre à virgule en une multiplication par un nombre entier (et inversément)


| Mult → Div     | Div &rarr; Mult |                                                  |
|----------------|-----------------|--------------------------------------------------|
| · 0,1 → : 10   | : 0,1 → · 10    | Multiplier par 0,1 devient diviser par 10 (et inversement) |
| · 0,25 → : 4   | : 0,25 → · 4    | Multiplier par 0,25 devient diviser par 4 (et inversement) |
| · 0,2 → : 5    | : 0,2 → · 5     | Multiplier par 0,2 devient diviser par 5 (et inversement)  |
| · 0,5 → : 2    | : 0,5 → · 2     | Multiplier par 0,1 devient diviser par 10 (et inversement) |
| · 0.01 → : 100 | : 0.01 → · 100  | Multiplier par 0,1 devient diviser par 10 (et inversement) |

## Multiplication et division par 10, 100, 1000...

Multiplier par 10, 100, 1000 revient à décaler la virgule vers la droite d'autant qu'il y a de '0'.

P.ex:

* $1,3 · 10 = 13$ (décaler la virgule de 1 pas)
* $12,74 · 1000 = 12740$ (décaler la virgule de 3 pas)

Diviser par 10, 100, 1000 revient à décaler la virgule vers la gauche d'autant qu'il y a de '0'.

P.ex:

* $1,3 : 10 = 0,13$ (décaler la virgule de 1 pas)
* $12,74 : 1000 = 0,012740$ (décaler la virgule de 3 pas)

## Associer des nombres

En respectant les règles d'associabilité de l'addition et de la multiplication, tu peux réarranger tes calculs pour les rendre plus facile:

* $13 + 5 + 7 = 13 + 7 + 5 = 20 + 5 = 25$
* $4 \cdot 3 \cdot 25 = 4 \cdot 25 \cdot 3 = 100 \cdot 3 = 300$

En associant des groupes de multiplication :

* $8\cdot3,2 + 2 \cdot 3,2 = (8+2)\cdot3,2=10 \cdot 3,2 = 32$

## Trouver des nombres premiers

Jusqu'à 49 ($7·7$), il suffit de vérifier si un nombre est divisible par 2, 3 ou 5. Si ce n'est pas le cas, c'est un nombre premier.

Jusqu'à 121 ($11 \cdot 11$), il faut vérifier **en plus** si c'est un multipe de 7 ($7\cdot11=77$; $7\cdot13=91$; $7\cdot17=119$). Si ces n'est pas le cas, c'est un nombre premier.
