---
title: NO 9.1 - Divisibilite
author: Charles Roduit
date: 2023
---

# ✅ Critères de divisibilité


!!! objectifs "Objectif"

    - [x] Je sais si un nombre est divisible par 2,3,4,5,9,10 et 25 en utilisant les critères de divisibilité.

[Vidéo sur les critères de divisibilité](https://www.youtube.com/watch?v=BJDE6uOrmYQ)

## A quoi ça sert ?

A déterminer si un nombre est divisible par un autre nombre sans faire le calcul.

Par exemple : "Est-ce que 4794 est divisible par 3 ?"

Sans faire la division, je sais que la réponse est "Oui".

!!! exercices 

    - NO27(L) &rarr; Arrangement en rectangle
    - NO28(L) &rarr; Remplacer un chiffre dans un nombre
    - NO29(F) &rarr; Tableau divisible ?
    - NO32(F) &rarr; Le triangle de Pascal0
    - NO36(L) &rarr; Dimentions entières
    - NO37(L) &rarr; Mon âge
    - NO41(L) &rarr; Métro
    - NO72(F) &rarr; 

!!! success "Les critères de divisibilité"

    Un nombre naturel est divisible par :
    
    * **2 s'il se termine par 0; 2; 4; 6 ou 8 (nombres pairs)**,
       ex.: *26; 34; 1548*.
    * **3 si la somme de ses chiffres est divisible par 3**,
      ex.: *27; 120; 1722*.
    * **4 si le nombre formé par les deux derniers chiffres est divisible par 4**,
       ex.: *24; 124; 2824*.
    * **S s'il se termine par 0 ou 5**,
       ex.: *25; 30; 1545*.
    * **9 si la somme de ses chiffres est divisible par 9**,
       ex.: *27; 171; 1755*.
    * **10 s'il se termine par 0**,
       ex.: *20; 30; 1540*.
    * **25 s'il se termine par 00; 25; 50 ou 75**,
       ex.: *25; 375; 1525*.
