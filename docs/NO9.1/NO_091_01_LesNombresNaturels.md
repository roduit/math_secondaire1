---
title: NO 9.1 - Les nombres naturels
author: Charles Roduit
date: 2023
---

# 🧠 Les nombres naturels

Pour la première semaine de la rentrée, nous allons réactiver les connaissances de math.

!!! objectifs "Objectif"

    - [x] Savoir sa table de multiplication (1 à 12)
    - [x] Savoir écrire correctement les calculs
    - [x] Faire des calculs de tête
    - [x] Connaître le vocabulaire des opérations de base
    
!!! definition "Définition : les nombres naturels"

    Les nombres naturels sont les nombres qui permettent de compter des objets concrets :
    1 pomme; 3 ananas; 27 astéroïdes; 0 fourchette.
    
    Un nombre naturel est donc entier et positif (avec le 0, il est positif **et** négatif).
    
    On le note $\mathbb{N}$ (N majuscule avec la diagonale doublée)

## Égalité

Ce qui se trouve à gauche et à droite d'une égalité doit effectivement être égal. A l'exercice NO4(L), Stéphane ne respecte pas l'égalité dans son écriture.

## Calculs de tête

Nous avons exercé en classe et en devoir les calculs.

!!! warning "Attention!"
    Ils sont à faire *sans* la machine à calculer. Regarde la section [Trucs et astuces](./NO_091_99_TrucsEtAstuces.md) pour t'aider.
    
!!! info
    Si tu as de la difficulté à faire les calculs, c'est comme le sport :

    👉 Il faut connaître les gestes de base 

    En mathématique, un des gestes de base est la table de multiplication. Apprends la bien et le reste sera plus facile. 💪Il faut s'entrainer💪 

Quelques exercices supplémentaires que tu peux faire pour être sûr de maitriser :

NO7(L) NO10(L) / NO11(L) / NO12(L) NO14(L)


!!! info "Pour la suite...."

    Voir AM p.22-25

## Poser une addition

[Video addition](https://www.youtube-nocookie.com/embed/ytLe8aUq2ZM)

!!! methode "Additionner des nombres décimaux"

    **Méthode** (AM, p.23)
    
    Exemple : 255,45 + 33,9 + 7,2 = ?

    1. **Calculer une approximation du résultat** (cela te permettra de vérifier ton résultat final).
    2. **Écrire** les termes de la somme en alignant les unités.
    3. **Effectuer** l'addition en colonne \*\*en commençant par la droite\*\* en notant les retenues sur le haut de la colonne suivante.
    4. **Vérifier** que la somme corresponde à l'approximation au point 1.  
    
## Poser une soustraction :

[Vidéo soustraction](https://www.youtube.com/watch?v=CFKUxlh6R9s)

!!! methode "Soustraire des nombres"

    **Méthode** (AM, p.23)
    
    1. **Calculer une approximation du résultat** (cela te permettra de vérifier ton résultat final).
    2. **Écrire** les termes de la soustraction en alignant les unités.
    3. **Effectuer** les soustractions en colonne en commençant par la droite.
    4. **Vérifier** que le résultat corresponde à l'approximation au point 1.    

## Poser une multiplication :

[Vidéo multiplication](https://www.youtube.com/watch?v=4YQi_icWTTI)

!!! methode "Soustraire des nombres"

    **Méthode** (AM, p.23)
    
    1. **Calculer une approximation du résultat** (cela te permettra de vérifier ton résultat final).
    2. **Écrire** la multiplication en colonnes en commençant par la droite (sans aligner les unités).
    3. **Effectuer** la multiplication en colonnes sans tenir compte des virgules.
    4. **Additionner** le nombre de décimales présente et placer la virgule en conséquence dans résultat obtenut.
    5. **Vérifier** que le résultat corresponde à l'approximation au point 1.

## Poser une division :

[Division en colonne](https://www.youtube.com/watch?v=RbkDd_p_EVU)

!!! methode "Soustraire des nombres"

    **Méthode** (AM, p.23)
    
    1. **Calculer une approximation du résultat** (cela te permettra de vérifier ton résultat final).
    2. **Faire du diviseur un nombre naturel** en le multipliant par 10, 100, ... et multiplier le dividende de la même manière.

        p.ex : $20,4 : 2,3 = 204 : 23$ **ou** $109,34 : 0.3 = 1093,4 : 3$
        
    5. **Effectuer** la division jusqu'à la virgue. Quand on l'atteind, mettre cette virgule dans le résultat, et continuer.
    6. **Vérifier** que la somme corresponde à l'approximation au point 1.

## Vocabulaire

!!! tip "A savoir"

    - Addition : La somme de termes toto<br>
        $Terme + Terme = Somme$
    - Soustraction : La différence de termes<br>
        $Terme - Terme = Différence$
    - La multiplication : Le produit de facteurs<br>
        $Facteur \cdot Facteur = Produit$
    - La division : Le quotient est la division du dividende par le diviseur<br>
        $\frac{Dividende}{Diviseur} = Quotient$
