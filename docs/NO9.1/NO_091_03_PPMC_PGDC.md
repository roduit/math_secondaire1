---
title: NO 9.1 - PPMC, PGDC et nombres premiers
author: Charles Roduit
date: 2023-06-10
---

# 🧜‍♀️️ PPMC / PGDC et nombres premiers

## 🌓 Les nombres premiers

[Vidéo](https://www.youtube.com/watch?v=g9PLLhnCv88)

!!! definition "Définition : Nombre premier"

    Un nombre premier est un nombre naturel qui a exactement deux diviseurs : 1 et lui-même.
    
!!! danger "Remarque"

    * 0 et 1 ne sont pas des nombres premiers.
    * Il y a une infinité de nombre premiers.

![](Illustrations/Definition_nbrPremier.png)
Liste des nombres premiers inférieurs à 1000: AM p.13

!!! exercices 

    * NO45(F) Le crible d’Eratosthène
    * NO46(F) Les premiers premiers
    * NO47(F) De X à Y

### Décomposition en facteurs de nombres premiers

[Vidéo](https://www.youtube.com/watch?v=RBE2wPIKagI)

Décomposition en facteur de nombres premiers = Atomisation du nombre

Les nombres premiers sont les constituants fondamentaux de tous les nombres, comme les atomes sont les constituants de la matière.

En mélangeant les atomes, on fabrique toute la matière présente dans l'univers (étoiles, planètes, roches, bois, ...)

En mélangeant (multipliant) les nombres premiers, on fabrique tous les nombres réels.

Décomposer un nombre en facteurs de nombres premiers est une manière de décortiquer un nombres, de voir de quoi il est fait. Avec cette décomposition, nous pourrons ensuite nous amuser avec ces nombres.


![](Illustrations/Propriete_decompostionFactPrem.png)

![](Illustrations/DecompositionFacteursPremiers_methode1.png)
![](Illustrations/DecompositionFacteursPremiers_methode2.png)

!!! exercices 

    * NO50(L) Décomposition
    * NO51(L) Toujours premier ?

## 🐭 PPMC

Le plus petit multiple commun.

### Pourquoi chercher un plus petit multiple commun ?

Dans certains problèmes, il est nécessaire de calculer le PPMC pour trouver la réponse.

Par exemple, quand

* un événement se répète à intervalles réguliers pour des personnes ou des choses différentes et que l'on veut connaître le moment où il se produira en même temps.
* on veut reconstituer un tout déjà divisé.
* il est question de temps ou d'intervalles réguliers.


Il permet aussi d'additionner des fractions, mais ça, on verra plus tard :-) .

### Comment chercher le PPMC ?

!!! methode "Méthode 1"
    
    Exemple: Trouver le PPMC de 5; 10 et 12

    1. **Ecris les multiples** de chacun des nombres:<br>
        $M_{5} = \{5; 10; 15; 20; 25; 30; 35; 40; 45; 50; 55; \mathbf{60}; 65; ...\}$<br>
        $M_{10} = \{10; 20; 30; 40; 50; \mathbf{60}; 70; ...\}$<br>
        $M_{12} = \{0; 12; 24; 36; 48; \mathbf{60}; 72; ...\}$
        
    2. **Identifie** le premier multiple commun non nul:<br>
        $ppmc(5; 10; 12) = 60$

!!! methode "Méthode 2"
    
    Exemple: Trouver le PPMC de 60 et 27   

    1. [Décomposer en facteur de nombres premiers](#les-nombres-premiers)<br>
        $60 = 2² \times 3 \times 5$<br>
        $27 = 3^3$
    2. Ecrit **tous les nombres premiers** que tu as trouvé, mais une seule fois<br>
        $2;  3;  5$
    3. Ecrit, pour chacun de ces nombres premiers **le plus grand exposant** qui correspond à ce nombre<br>
        $2²$; $3^3$; $5^1$
    4. **Effectue le produit** des nombres obtenus<br>
        $\begin{aligned}
        ppmc(60; 27) &= 2^2 \times 3^3 \times 5 \\
        &= 4 \times 9 \times 5 \\
        &=180
        \end{aligned}$

!!! exercices 

    Livre : NO52, NO54 (avec PGDC)

    Fiches : P.20 (faire le point)
    NO76 (avec d'autres notions)

## 🐘 PGDC

### C'est quoi ?

### Pourquoi chercher un PGDC ?

Il permet de diviser un nombre de personnes ou d'objets en groupements pour obtenir le plus grand nombre possible de groupements ou les plus gros groupements possibles.

On l'utilise lorsqu'il est question de partage sans reste ou de quantité.

Il permet aussi de rendre irréductible une fraction, mais ça on verra plus tard :-)

[//]:#(## Définition)
[//]:#(Un **diviseur** commun de plusieurs nombres naturels est un diviseur de chacun de ces nombres.)

[//]:#(## Exemple)
[//]:#(2 est un diviseur commun de 6 ; 24 et 172, car 2 divise chacun de ces trois nombres.)

[//]:#(## Propriété)

[//]:#(Quels que soient les nombres naturels donnés, ils ont au moins un diviseur commun. En effet, 1 est un diviseur de tous les nombres.)

!!! definition "Définition : PGDC"

    Le **pgdc** de plusieurs nombres naturels est le **p**lus **g**rand **d**iviseur **c**ommun de ces nombres.

### Exemple

5 est le pgdc de 10 ; 15 et 30. On écrit : pgdc (10 ; 15 ; 30) = 5.

### Comment choisir parmi ces deux méthodes

On choisit de préférence la première méthode lorsque les diviseurs de chacun
des nombres sont faciles à calculer.

!!! exercices

    NO41; NO42; NO43; NO55; NO56; 

[//]:# (** > Ensembles de nombres (p. 10), Nombres naturels (p. 12), Multiple, diviseur (p. 12) )
