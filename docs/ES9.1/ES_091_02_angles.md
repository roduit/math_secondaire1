# 📐 Les angles

## Nommer les angles ∢

Un angle définit par le croisement de deux demi-droites $AB$ et $AC$ en $A$ : $\widehat{BAC}$.
Le circonflex part de B, sommet en A et descend vers C.

De la même manière si on parle de la droite $b$ et de la droite $a$ qui se croisent en E : $\widehat{aEb}$.

On peut aussi nommer les angles avec les lettres grecs : $\alpha$ (alpha), $\beta$ (beta), $\gamma$ (gamma), $\delta$ (delta), $\epsilon$ (epsilon),...

## Classement des angles

![](./Video/angles.gif)

| nom     | angle     |
|----------|--------:|
|angle nul| angle=0°  |
|angle aigu | 0° < angle < 90° |
|angle droit| angle = 90°|
|angle obtus|90° < angle < 180°|
|angle plat |angle = 180°|
|angle rentrant|180° < angle < 360° |
|angle plein|angle = 360°|

