---
title: Figures géométriques planes
authors: Charles Roduit
date: 2023-06-28
---

# :chart_with_upwards_trend: Figures géométriques planes.

## :dart: Objectifs

??? success "Reconnaître, nommer, décrire des figures planes selon leurs propriétés (symétrie-s interne-s, côtés, angles, somme des angles, diagonales) et construire des angles, triangles, quadrilatères, cercles"

    * [x] Connaître le vocabulaire et les concepts lié aux figures planes : droite, demi- droite, segment, distance entre deux points, entre un point et une droite, entre deux droites parallèles, droites parallèles, sécantes, perpendiculaires, médiatrice, bissectrice, hauteur, côté, sommet, base, hauteur, longueur, largeur, grande et petite base, diagonale, cercle, arc de cercle, centre, corde, rayon, diamètre, cercle inscrit et circonscrit, axe et centre de symétrie, périmètre, aire.
    * [x]  Connaître la définition et les propriétés des triangles et quadrilatères particuliers pour pouvoir les identifier, les classer, les construire ou déduire certaines informations manquantes :
        - triangles : quelconque, rectangle, isocèle, équilatéral, rectangle isocèle
        - quadrilatères : trapèze, trapèze rectangle et isocèle, parallélogramme, carré, losange, rectangle.
    * [x] Connaître et pouvoir utiliser l’inégalité triangulaire pour déterminer si un triangle existe ou pas.
    * [x]  Noter un angle ou pouvoir interpréter sa notation.
    * [x]  Construire un angle de 60° et 90° à l’aide d’un compas et d’une règle graduée uniquement ; Sur cette base et avec les mêmes instruments, en déduire la construction de ceux de 30°, 45° et 120°.
    * [x]  Construire un triangle en connaissant ses trois côtés, deux côtés et un angle, un côté et deux angles.
    * [x]  Construire un quadrilatère en fonction de ses propriétés et en connaissant certaines de ses mesures (côtés, angle, diagonale).
    * [x] Calculer l’angle au centre et l’angle intérieur d’un polygone régulier.

??? success "Reconnaître et nommer des angles (aigu, obtus, droit, plat, rentrant)"
    * [X] Donner le type d’un angle en fonction de sa mesure ou pouvoir le représenter en fonction de son type : angle nul, aigu, droit, obtus, plat, rentrant, plein.
     
??? success "Estimer, comparer, classer et mesurer des angles en degrés"
    * [x] Estimer à vue d’œil la valeur d’un angle en reconnaissant son type.
    * [x] Construire et mesurer des angles avec un rapporteur.    
    * [x] Faire des calculs d’angles en utilisant :
        - les définitions de l’angle aigu, obtus, rentrant, plein, plat, droit,
        - les propriétés de la somme des angles d’un triangle et d’un quadrilatère.
        - les propriétés des polygones, de la médiatrice et de la bissectrice.

??? success "Reconnaître, nommer, décrire des propriétés et construire :"

    - des droites parallèles, des droites perpendiculaires
    - une hauteur, une médiatrice, une bissectrice
    - cercles inscrit et circonscrit

    * [x] Construire des droites perpendiculaires et parallèles à l’aide d’une équerre et d’une règle.
    * [x] Définir la médiatrice et la bissectrice et connaître ses propriétés.
    * [x] Construire la médiatrice d’un segment avec un compas et une règle non graduée
    * [x] Tracer les trois hauteurs d’un triangle.
    * [x] Construire la bissectrice d’un angle avec une règle et un compas.
    * [x] Utiliser les propriétés de la médiatrice et de la bissectrice pour déterminer des lieux géométriques et construire le cercle circonscrit et inscrit à un triangle.

??? success "Représenter des figures planes par un croquis et/ou un dessin à l’échelle (y compris 1:1)"
    * [x] Différencier un croquis d’un dessin à l’échelle.
    * [x] Être capable de réaliser un croquis et de le coder ou d’interpréter un codage.
    * [x] Pouvoir utiliser une échelle simple : 1 : 1, 1 : 10, 1 : 100, …

!!! competence "Compétences visées"
    - Mobiliser les savoirs, savoir-faire et savoir être pour résoudre des problèmes en lien avec les figures géométriques. 
    - Développer des stratégies de recherche afin de résoudre des problèmes
    - Présenter les solutions afin que les autres puissent s’approprier ma démarche (explications préalables, grandeur cherchée, unités, calculs, réponse).
    - Développer des stratégies d’apprentissages afin de s’approprier les savoirs et savoir-faire en jeu et pouvoir les mobiliser en situation.
    - Vérifier sa solution ou au moins sa plausibilité. 

[## :bubbles:]: #
[Rappelle-toi des différents mouvements pour tracer :]: #
[]: #
[??? tip "des droites"]: #
[    test]: #
