# 🚵 Planification

C'est une prévision de planificaiton. Bien sûr, selon les activités, les questions, le travail, cette planificaiton sera adaptée durant le cours.

## Les points et les droites

!!! objectifs "Objectifs travaillés"
    - [ ] Je connais le vocabulaire et les concepts liés au plan.
          *droite, demi-droite, segment, distance entre deux points, distance entre un point et une droite, distance entre deux droites parallèles, droites parallèles, sécantes, perpendiculaires, médiatrice, bissectrice,*
    - [ ] Je sais construire des droites perpendiculaires et parallèles.
    
!!! exercices "Exercices concernés"
    - ES1-9

## Les angles

!!! objectifs "Objectifs travaillés"
    - [ ] Je sais lire et utiliser les différentes notations d'un angle.
    - [ ] J'identifie le type d'angle en fonction de sa mesure ou de sa représentation et je sais représenter les différents types d'angles.
    - [ ] Je peux estimer à l'oeuil nu la valeur d'un angle.
    - [ ] Je suis capable de construire et de mesurer des angles avec un rapporteur.
    - [ ] Je sais calculer des angles et justifier ses déductions en utilisant :
        - les valeurs de l’angle plein, plat, droit
    
!!! exercices "Exercices concernés"
    - ES 10-20 (Les angles)
    
## Les lieux géométriques

!!! objectifs "Objectifs travaillés"
    - [ ] Définir la médiatrice et la bissectrice et connaître ses propriétés.
    - [ ] Construire la médiatrice d’un segment avec un compas et une règle non graduée
    - [ ] Utiliser les propriétés de la médiatrice et de la bissectrice pour déterminer des lieux géométriques
    - [ ] construire le cercle circonscrit et inscrit à un triangle.
    
!!! exercices "Exercices concernés"
    - ES 21-35 (Médiatrice, bissectrice)
    - ES 69-60 (cercles circonscrits et inscrits)
    
## Les dictées géométriques
!!! objectifs "Objectifs travaillés"
    - [ ] Je suis capable de mobiliser mes connaissances pour réaliser un dessin technique complexe.
    
!!! exercices "Exercices concernés"
    - ES 36-40 (
# Deuxième test

## Les triangles

!!! objectifs "Objectifs travaillés"

    - [ ] Je connais la définition et les propriétés des triangles particuliers et peux les identifier, les classer, les construire ou déduire certaines *(triangles : quelconque, rectangle, isocèle, équilatéral, rectangle isocèle*).
    - [ ] Connaître et pouvoir utiliser l’inégalité triangulaire pour déterminer si un triangle existe ou pas
    - [ ] Construire un triangle en connaissant ses trois côtés, deux côtés et un angle, un côté et deux angles
    - [ ] Tracer les trois hauteurs d’un triangle

!!! exercices "Exercices concernés"
    - ES41-54 (Généralités)
    - ES56-61 (Droites remarquables d'un triangle)
    - ES62-73 (Analyse d'un triangle)
    
## Les quadrilatères

!!! objectifs "Objectifs travaillés"
    - [ ] Je connais la définition et les propriétés des quadrilatères particuliers et peux les identifier, les classer, les construire ou déduire certaines informations manquantes *(trapèze quelconque, trapèze rectangle et isocèle, parallélogramme, carré, losange, rectangle)*.
    - [ ] Je peux construire un quadrilatère en fonction de ses propriétés et en connaissant certaines de ses mesures (côtés, angles, diagonales).
    - [ ] Je sais calculer des angles et justifier ses déductions en utilisant:
        - les propriétés de la somme des angles d’un triangle et d’un quadrilatère
        - les propriétés des triangles et quadrilatères, de la médiatrice et de la bissectrice.
    - [ ] Différencier un croquis d’un dessin à l’échelle.

!!! exercices "Exercices concernés"
    - ES 75-77 (Réactivations)
    - ES 78-86 (Reconnaître, nommer, décrire et construire des quadrilataires)
    - ES 87-96 (Problèmes)
## Partie 3 - 
