# 🧭 Lieux géométriques

!!! definition "Définition: un lieu géométrique"

    En mathématiques, un lieu géométrique est un point ou un ensemble de points remplissant une condition en fonction de son axe ou de son nombre de points, données par un problème de construction géométrique (par exemple à partir d'un point mobile sur une courbe) ou par des équations ou inéquations reliant des fonctions de points (notamment des distances).

[Wikipedia.org](https://fr.wikipedia.org/wiki/Lieu_g%C3%A9om%C3%A9trique)

## La bissectrice

!!! definition "Définition: bissectrice"

    La bissectrice d’un angle est la droite qui le partage en deux angles isométriques.

!!! tip "Construction d'une bissectrice"

    ![Tracer la bissectrice](./Illustrations/Construction_Bissectrice.gif)

!!! info "Propriétés"

    La bissectrice d’un angle est aussi :

    * l’ensemble des points situés à égale distance des côtés de l’angle ;
    * l’axe de symétrie de cet angle.

[### Exemple]: #
[]: #
[$B_{1}A_{1} = B_{1}C_{1}$ ]: #
[]: #
[B₁A₁ = B₁C₁]: #
[]: #
[B₂A₂ = B₂C₂]: #
[]: #
[![(./Illustrations/Propriete_bissectrice.png)]: #

## La médiatrice

!!! definition "Définition: la médiatrice"

    La médiatrice d’un segment est la droite perpendiculaire à ce segment et qui le coupe en son milieu.

!!! tip "Construction d'une médiatrice"

    ![Tracer la médiatrice](./Illustrations/Construction_Mediatrice.gif)


!!! info "Propriétés"

    * La médiatrice d’un segment est l’ensemble des points à égale distance des extrémités de ce segment.
    * La médiatrice d’un segment est un axe de symétrie de ce segment.

[### Exemple]: #
[M, N et Q sont des points à égale distance]: #
[de A et de B (MA = MB ; NA = NB ; QA = QB).]: #
[La droite d est la médiatrice du segment AB et]: #
[donc un axe de symétrie de ce segment.]: #
[ ]: #
[![(./Illustrations/Propriete_mediatrice.png)]: #
