---
title: 🏡 Espace : Figures géométriques planes
---

# ES 9.1 Espace Figures géométriques planes

## 🎯 Objectifs 1/2

### 1. Les points et les droites

- [ ] Je connais le vocabulaire et les concepts liés au plan.
      *Droite, demi-droite, segment, distance entre deux points, distance entre un point et une droite, distance entre deux droites parallèles, droites parallèles, sécantes, perpendiculaires, médiatrice, bissectrice,*
- [ ] Je sais construire des droites perpendiculaires et parallèles.

### 2. Les angles

- [ ] Je sais lire et utiliser les différentes notations d'un angle.
- [ ] J'identifie le type d'angle en fonction de sa mesure ou de sa représentation et je sais représenter les différents types d'angles.
- [ ] Je peux estimer à l'œil nu la valeur d'un angle.
- [ ] Je suis capable de construire et de mesurer des angles avec un rapporteur.
- [ ] Je sais calculer des angles et justifier ses déductions en utilisant les valeurs de l’angle plein, plat et droit.

### 3. Les lieux géométriques

- [ ] Je sais définir la médiatrice et la bissectrice et connais ses propriétés.
- [ ] Je suis capable de construire la médiatrice d’un segment avec un compas et une règle non graduée.
- [ ] Connaissant les propriétés de la médiatrice et de la bissectrice je peux déterminer des lieux géométriques.
- [ ] Je suis capable de construire le cercle circonscrit et inscrit à un triangle.
    
### 4. Les dictées géométriques

- [ ] Je suis capable de mobiliser mes connaissances pour réaliser un dessin technique complexe.


## 🎯 Objectifs 2/2

### 1. Les triangles

- [ ] Je connais la définition et les propriétés des triangles particuliers et peux les identifier, les classer, les construire ou déduire certaines *(triangles : quelconque, rectangle, isocèle, équilatéral, rectangle isocèle*).
- [ ] Je connais l’inégalité triangulaire et suis capable de l'utiliser pour déterminer si un triangle existe ou pas.
- [ ] Je peux construire un triangle en connaissant ses trois côtés, deux côtés et un angle, un côté et deux angles.
- [ ] Je sais tracer les trois hauteurs d’un triangle.
    
### 2. Les quadrilatères

- [ ] Je connais la définition et les propriétés des quadrilatères particuliers et peux les identifier, les classer, les construire ou déduire certaines informations manquantes *(trapèze quelconque, trapèze rectangle et isocèle, parallélogramme, carré, losange, rectangle)*.
- [ ] Je peux construire un quadrilatère en fonction de ses propriétés et en connaissant certaines de ses mesures (côtés, angles, diagonales).
- [ ] Je sais calculer des angles et justifier ses déductions en utilisant:
    - les propriétés de la somme des angles d’un triangle et d’un quadrilatère
    - les propriétés des triangles et quadrilatères, de la médiatrice et de la bissectrice.
- [ ] Je différencie un croquis d’un dessin à l’échelle.

