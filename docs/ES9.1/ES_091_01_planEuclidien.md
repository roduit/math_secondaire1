# 📏 Les points et les droites

## Le point

!!! defintion "Définition: le point"
    Un point est un emplacement sur un plan. Il n'a ni surface, ni dimention.
    
- On note le point comme un croisement de deux droites (par une croix).
- On nomme le point avec une lettre majuscule.

## Les 3 types de droites

!!! definition "Définition: Les droites"

    Il y a trois type de droites :
    
    les droites, les demi-droites et les segments de droites.

### La droite

!!! definition "Définition: La droite"
    La droite est un ensemble de points allignés, de longueur infinie et d'épaisseur nulle.
    
    Une droite est appelée soit par deux points par lesquelles elle passe (nommés par une lettre majuscule), soit par une lettre minuscule.
    
La droite suivante est appelée "droite $AB$", car elle passe par les points $A$ et $B$. On peut aussi l'appeler "droite $f$".

![Droite](./Illustrations/Droite_AB.png)

### La demi-droite

!!! definition "Définition: La demi-droite"
    Une demi-droite est une portion de droite limitée d'un seul côté par un point, son origine.
    
    Une demi-droite est appelée soit par les deux points par lesquelles elle passe, en commençant par l'origine, soit par une lettre minuscule.

La droite suivante est appelée "demi-droite $AB$", car elle a son origine sur le point $A$ et passe par le point $B$. On peut l'appeler aussi "demi-droite Ad" car c'est la portion de la droite $d$ qui passe par $A$, ou demi-droite $[Ad)$, le crochet indiquant l'origine de la droite, la parenthèse indiquant que l'on va jusqu'à l'infini du côté B.

![Demi-droite](Illustrations/DemiDroite_AB.png)


### Le segment de droite

!!! definition "Définition: Le segment de droite"
    Un segment de droite est une portion de droite limitée entre deux points, ses extrémités.


La droite suivante est appelée "segment de droite AB" ou "segment de droite [AB]", les crochets indiquant les limites du segment de droite.

![Segment de droite](Illustrations/SegmentDroite_AB.png)

## Relations entre les droites

Deux droites peuvent être...
(parallèles/sécantes/perpendiculaires)

### Parallèles

!!! definition "Définition: droites parallèles"

    Deux droites sont parallèles si elles ne se croisent jamais. Elles ont soit aucun point commun, soit elles sont confondues (tous leurs points sont communs).
    

$d // f$ indique que $d$ et $f$ sont parallèles

!!! tip "Tracer une parallèle"

    [Une vidéo explicative](https://www.youtube.com/watch?v=0J-qLZArCmo)


### Sécantes

!!! definition "Définition: droites sécantes"
    Deux droites sont sécantes si elles se croisent en un point.

### Perpendiculaires


!!! definition "Définition: droites perpendiculaires"

    Deux droites sont perpendiculaires si elles se croisent selon un angles droit (90°). C'est un cas particulier de droites sécantes.

$g \perp h$ indique que $g$ et $h$ sont perpendiculaires.

!!! tip "Tracer une perpendiculaire"

    [Une vidéo explicative](https://www.youtube.com/watch?v=0J59aZmTwJA)
